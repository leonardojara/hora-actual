<%-- 
    Document   : index
    Created on : 28-mar-2020, 12:54:39
    Author     : Leonardo.Jara
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.time.LocalTime" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <%! LocalTime hora; %>
    <%
            hora = LocalTime.now();
    %>
    <body>
        <h1>la hora es : <%=hora %></h1>
    </body>
</html>
